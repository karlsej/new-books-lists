<style>
.hidden {display:none;}
.access-hidden {position:absolute; left:-99999px;}
.explanatory {font-size:1.2em; margin-bottom:0 !important;}
#new-books-section {position:relative; width:100%; margin-bottom:36px;}
.new-book-link {margin-left:-10px;}
#new-books-section div {width:100%; float:none; }
#callno-form {margin-bottom:20px;}
.list-form label {font-size:1.3em;}
#callno-browse, #kw-search {font-size:1.1em; max-width:100%;}
#callno-browse {display:block;}
#kw-search, #extend-query {padding:.4em .2em; margin-bottom:10px;}
#kw-search-form label {display:block;}
#newwins, #full-list {float:right; clear:both;}
#pager {float:none; text-align:center; margin:1em auto 0; clear:both;}
#pager ul {margin:8px auto; padding:0;}
#pager li {display:inline; margin:0 8px;}
#pager .short {position:absolute; left:-999999px;}
#pager .btn {cursor:pointer; font-weight:bold; background: #FFCE00;    color: #8A0028;   border: none; border-radius: 4px; width:50px; padding:4px;}
#pager .btn:hover {    background: #FDE373;color: #4D4D4D;}
#pager span {margin-left:.5em;}
#pager span:first-of-type {margin-left:0;}
#pager a:link, #pager a:visited {background:#ddd; border:1px solid #eee; padding:2px 6px; text-decoration:none;}
#pager a:hover, #pager a:focus, #pager .active {background:#ccc; color:#fff; text-decoration:none;}
.btn:nth-of-type(1), .btn:nth-of-type(4) {display:none !important;} 
.btn:nth-of-type(3) {position:absolute; bottom:-28px; right:20%; display:block !important; }
.btn:nth-of-type(2) {position:absolute; bottom:-28px; left:20%; display:block !important; }
.subj-list li {display:inline; margin-right:1em !important;}
.subj-list li a {font-weight:normal; font-size:90%;}
.loc-callno, .item-callno {display:block; margin:.4em 0 0 0 !important;}
.new-book {margin-bottom:10px; padding-top:10px !important; clear:both; font-size:110%; list-style-type:none;}
.author {display:block; margin-top:.4em !important; padding-bottom:.1em !important;}
.jacket {float:left; max-height:140px; padding:8px 12px 8px 0 !important;}
.dvd-icon {margin:4px 0 0 8px !important;}
#no-results {font-size:1.2em; margin-top:60px; text-align:center;}
#show-all { padding:10px; float:left; }
#send-list {float:right; background:transparent; color:#8A0028; font-weight:bold; border:none;}
#send-list:hover, #send-list:focus {color:#4D4D4D; text-decoration:underline;}
	#img-attribution {font-style:italic; position:absolute; bottom:10px;}
	#img-attribution img {position:relative; top:4px; left:4px;}
.extend {font-size:1.1em;}
#extend-form div {float:none;}
.textwidget ul {padding-left:0 !important;}
@media screen and (max-width: 600px) {
    #newwins {float:none; text-align:right;}
    }
    </style>
<?php
/*
// show errors for debugging
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
*/
if ( !defined('DONOTCACHEPAGE') ){
define('DONOTCACHEPAGE',true);
}
function writeItems($auth, $ti, $subj, $l, $ca, $ib, $img, $type) {
    echo '<li class="new-book">';

    echo $ti;
    if (!empty($auth)) {
        echo '<div class="author">' . $auth . '</div>';
        }
    echo '<div class="loc-callno"> ' . $l . ' <span class="callno">' . $ca . '</span></div>';
	if ($type !== 'complete') {
		echo $ib;
		echo $img;
	}
	if ($type !== 'complete') {
		if ($subj[0] !== '') {
            echo '<ul class="subj-list">';
            for ($n = 0; $n < count($subj); $n++) {
                $subject = $subj[$n];
                echo '<li>' .$subject .' </li>';
                }
            echo "</ul>\n";
            }
	}
        echo "</li>\n";
    }
$query = $_GET['query'];
$callNoPrefix = $_GET['callno'];
$regex = $_GET['match'];
$sort = $_GET['sort'];
$regex = str_replace('\\\\', '\\', $regex); // weird - need to escape backslashes in str_replace
// echo $regex;
$list = $_GET['list'];
// $yearPub = $_GET['year'];
$media = $_GET['media'];
$scLoc = $_GET['location'];
$complete = $_GET['complete'];
if (isset($media)) {
    $listLabel = 'DVDs';
    }
// get path so that can reuse this script
$uri = $_SERVER["REQUEST_URI"];
// echo '<br>uri is: ' . $uri . '<br>';
$page = preg_replace('/\/\?.*/', '', $uri);
// echo $page;
// echo '<br>Page string is: ' . $page . '<br>';
$endPage = preg_match('/\/([\w-]+)(\/)?$/', $page, $pageNames); // get last part of path
$pageName = $pageNames[0];
$pageName = str_replace('/', '', $pageName); // this will be same as json file name
$pageName = preg_replace('/\/\?.*/', '', $pageName);
// echo '<br> page name is ' . $pageName . '<br>';
// echo '<br> position is ' .strpos($pageName, 'ebook');
if (strpos($pageName,'ebook') !== false) {
    $ebooks = 'yes';
    }
    else {
        $ebooks = 'no';
        }
// echo '<br>ebooks is ' .$ebooks;
$jsonRoot = 'http://wserver.scc.losrios.edu/~library/new-acquisitions/lists/';
$bookspage = 'http://www.scc.losrios.edu' . $page;
// $bookspage = 'index.php';
$file = $jsonRoot . $pageName . '.json';
// $file = '2015-may-july.json'; // for testing
// echo '<br>' .$file;
// get file date so that locations can be hidden after a month or two. for some reason I need to create the date, turn it into a strong and then back into a time? otherwise I get error that object couldn't be converted to int

$h = get_headers($file, 1);
// $h = implode($h);
// echo $h;
$fileDate = NULL;
if (!(!$h || strstr($h[0], '200') === false)) {
    $fileDate = new \DateTime($h['Last-Modified']);//php 5.3
    }
$dateString = $fileDate->format('Y-m-d');
// echo $fileDate . "<br>";
if (strtotime($dateString) < strtotime('2 months ago')) {
    // echo '<br>old file';
    $fileAge = 'old';
    }
    else {
        $fileAge = 'young';
        // echo '<br>not old';
        }
        
// copy file content into a string var
$json_file = file_get_contents($file);
// convert the string to a json object
$data = json_decode($json_file);
// copy the posts array to a php var
$books = $data->books;
echo '<a id="full-list" class="hidden"  href="' . $bookspage .'?complete">View complete list</a>';
?>
<form id="newwins">
    <input type="checkbox" id="target-setting"><label for="target-setting"> Open links in new windows</label>
</form>
<form class="list-form" id="kw-search-form" action="<?php echo $bookspage; ?>" method="get">
    <label for="kw-search">Search for new items by keyword (e.g. subject, author, title):</label>
    <input type="text" name="query" id="kw-search">
    <input type="submit" value="search">
</form>
<form class="list-form" action="<?php echo $bookspage; ?>" id="callno-form" method="get">
    <label for="callno-browse">Browse by call number: </label>
    <select name="callno" id="callno-browse">
        <option selected="selected" value="">Select a Call Number Range</option>
        <option value="A">A - General Works</option>
        <option value="B">B - Philosophy, Psychology, Religion</option>
	<option value="C">C - Auxiliary Sciences of History</option>
	<option value="D">D - World History and History of Europe, Asia, Africa, Australia, New Zealand, etc.</option>
	<option value="E">E - History of the Americas</option>
	<option value="F">F - History of the Americas</option>
	<option value="G">G - Geography, Anthropology, Recreation</option>
	<option value="H">H - Social Sciences</option>
	<option value="J">J - Political Science</option>
	<option value="K">K - Law</option>
	<option value="L">L - Education</option>
	<option value="M">M - Music and Books on Music</option>
	<option value="N">N - Fine Arts</option>
	<option value="P">P - Language and Literature</option>
	<option value="Q">Q - Science</option>
	<option value="R">R - Medicine</option>
	<option value="S">S - Agriculture</option>
	<option value="T">T - Technology</option>
	<option value="U">U - Military Science</option>
	<option value="V">V - Naval Science</option>
	<option value="Z">Z - Bibliography, Library Science, Information Resources (General)</option>
    </select>
    <input type="submit" value="submit" class="access-hidden">
</form>
<?php
$homeMessage = '';
if (strpos($uri, '?') === false) {
    $homeMessage = '<p class="explanatory">Below are a few sample items. To view more, use the search or browse controls above, or <a href="' . $bookspage . '?complete">view the entire list</a>.</p>';
    
}
echo $homeMessage;
echo '<div id="new-books-section">';
echo '<button id="send-list" type="button"><img src="//www.scc.losrios.edu/library/wp-content/plugins/socials-ignited/images/square/default/16/email.png" alt=""> Email this list</button>';
echo '<button id="show-all" class="hidden button" >View all results one page</button>'; 
echo "<div id=\"pager\"></div>\n";
echo '<ul id="scc-new-book-list">';

// sortinng functions http://stackoverflow.com/a/4282423
function cmpauth($a, $b) // 
{
    return strcmp($a->AUTH, $b->AUTH);
}
function cmptitle($a, $b) 
{
    return strcmp($a->TITLE, $b->TITLE);
}
function cmpcallno($a, $b)  
{
    return strcmp($a->CALL, $b->CALL);
}
if (($sort === 'alpha') || (strpos($uri, '?') === false)) {
    usort($books, 'cmptitle');
}

else {
    usort($books, 'cmpcallno');
}
foreach($books as $book) { // store JSON values in variables
    $callno = $book -> CALL;
    $location = $book -> LOC;
    $author = $book -> AUTH;
//	$book -> ED = array();
    $editor = implode('; ',$book -> ED);
    if ($author === '') {
        $author = $editor;
    }
    $author = str_replace(', author.', '', $author); // should probably take care of this when making JSON file
    $author = str_replace(' author.', '', $author);
    $title = $book -> TITLE;
    $subjects = $book -> SUBJ;
    $year = $book -> YEAR;
    $recordno = $book -> REC;
    $isbn = $book -> ISBN;
    if (strlen($isbn) === 9) {
        $isbn = '0' . $isbn;
    }
    $dvd = '';
    $edsDB = 'cat01047a';
    $dbName = 'OneSearch'; // for title attributes
    $accessNo = 'lrois.' . $recordno; // for our catalog items
    $imgURL = '//covers.openlibrary.org/b/isbn/' . $isbn . '-M.jpg';
    switch ($location) {
        case 'scbk':
        $loc = '3rd Floor Circulating';
        break;
        case 'scnew':
        $loc = '2nd Floor New Books';
        break;
        case 'sv2h':
        $loc = 'Reserve Collection';
        break;
        case 'sv2hl':
        $loc = 'Reserve Collection';
        break;
		case 'sv1d':
        $loc = 'Reserve Collection';
        break;
		case 'sv3d':
		$loc = 'Reserve Collection';
		break;
        case 'sv1w':
        $loc = 'Reserve Collection';
        break;
        case 'srref':
        $loc = '2nd Floor Reference Shelves';
	break;
	case 'scbro':
	$loc = '2nd Floor Paperbacks';
	break;
	case 'smdvd':
	$loc = 'Circulating DVDs';
	$dvd = '<img src="http://scc.losrios.edu/library/wp-res/dvd.png" alt="dvd" class="dvd-icon" height="16" width="16">'; // icon that displays after dvd items
	break;
	case 'smres':
	$loc = 'Reserve Collection';
	$dvd = '<img src="http://scc.losrios.edu/library/wp-res/dvd.png" alt="dvd" class="dvd-icon" height="16" width="16">';
	break;
        case 'scjez':
        $loc = '2nd Floor Easy Reading';
	break;
	case 'scjuv':
	$loc = '2nd Floor Juvenile';
	break;
	case 'ebk':
//	$loc = $year;
	$edsDB = 'nlebk';
	$imgURL = 'http://rps2images.ebscohost.com/rpsweb/othumb?id=NL$'. $recordno . '$PDF&s=d'; // reliable image source for ebsco ebooks
	$accessNo = $recordno;
	break;
		case 'wbebk':
		$loc = 'Ebook';
		break;
	case 'scdis':
	$loc = '3rd Floor Circulating'; // because they're not display books for very long
	break;
	default:
	$loc = '';
        }
	if ($fileAge === 'old') {
		$loc = '';
	}
	if (isset($media)) {
		$dvd = '';
	}
        if ($loc !== '') {
            $loc = $loc . ', '; // to precede call no
            }
        $heblink = 'http://0-hdl.handle.net.lasiii.losrios.edu/2027/heb.' . $recordno;
	$epRoot = 'search.ebscohost.com';
	if ($location === 'ebk') {
		$epRoot = '0-search.ebscohost.com.lasiii.losrios.edu';
	}
        $eplink = 'http://' . $epRoot . '/login.aspx?authtype=ip,guest&custid=sacram&groupid=main&profile=eds&direct=true&db=' . $edsDB .'&AN=' . $accessNo . '&site=eds-live&scope=site';
	if ($location === 'heb') {
            $dbName = 'Humanities eBook';
            $eplink = $heblink;
	$loc = $year;
            }
	$titleLine = '<a title="Go to item record in ' . $dbName . '" class="new-book-link" href="' . $eplink .'">' . $title .'</a>' .  $dvd;
	$isbnLine = '<div class="isbn hidden">' . $isbn . '</div>';
	if ($location === 'ebk') {
		$epub = str_replace('PDF', 'EPUB', $imgURL);
		$loc = $year;
	$imgsrc = '<img alt="" src="' . $imgURL . '" class="jacket" onerror="this.setAttribute(\'src\', \'' . $epub . '\'); this.removeAttribute(\'style\');  this.removeAttribute(\'onerror\');">';
        $imgLink = '<a class="new-book-link img-link" href=" ' .$eplink . '">' . $imgsrc . '</a>';
        // alternative link sources - dont' seem to do any better. should try Google books but requires more complicated js
        // $imgsrc = '<img alt="" src="http://covers.librarything.com/devkey/d8144a672ea3b2b10da882f090364d87/medium/isbn/' . $isbn . '" class="jacket">';
        // $imgsrc = '<img alt="" src="http://contentcafe2.btol.com/ContentCafe/jacket.aspx?UserID=ebsco-test&Password=ebsco-test&Return=T&Type=M&Value=' . $isbn . '" class="jacket">';
	}
	else {
		$imgLink = '';
	}
	if (isset($media)) {
            // would be good to put this in a function somehow... because need to repeat it frequently.
            if (($location === 'smdvd') || ($location === 'smres')) {
                writeItems($author, $titleLine, $subjects, $loc, $callno, $isbnLine, $imgLink, 'media');
                    }
                }
                elseif (!empty($query)) { // match query to author, title, subjects
                 //   writeItems($author, $titleLine, $subjects, $loc, $callno, $isbnLine, $imgLink, 'query', $query);
                    $query = strtolower($query);
                    $queryTrunc = str_replace('*', '', $query);
                    $queryTrunc = preg_replace('/s$/', '', $queryTrunc);
                    $authorLC = strtolower($author);
                    $titleLC = strtolower($title);
                    $subjectString = implode(' ', $subjects);
                    $subjectString = strtolower($subjectString);
                    // match these strings only at beginning of word, to avoid so many false positives
                    if ((strpos($authorLC, $queryTrunc)> -1) || (preg_match('/(^| )' . $queryTrunc . '/', $titleLC) === 1) || (preg_match('/(^| )' . $queryTrunc . '/', $subjectString) === 1)) {
                        writeItems($author, $titleLine, $subjects, $loc, $callno, $isbnLine, $imgLink, 'query');
                        }
                        }
                elseif (!empty($callNoPrefix)) { // match only beginning of call numbers. Note also they need to be capitalized.
                    if (strpos($callno, $callNoPrefix) === 0) {
                        writeItems($author, $titleLine, $subjects, $loc, $callno, $isbnLine, $imgsrc, 'callno');
                        }
                        }
		elseif (isset($list)) { // special lists. Only relies on call numbers. would be desirbale to incorporate keyword queries
                    $limit = '';
                    switch ($list)  {
                        case 'admj':
                            $cpattern = '/^(^HV6[1235][0-9][0-9][0-9]|HV[789][0-9][0-9][0-9]|KF9[234678])/';
                            $listLabel = 'Criminal Justice';
                            break;
			case 'bus':
			    $cpattern = '/^HD[12345678][123456789][123456789]|^HD[0][123][123456789]|^HD1[123][123456789][12345]|^H[GHIJ]|^HF5[12345789]|TS/';
			    $listLabel = 'Business, Management, Marketing';
			    break;
			case 'cis':
			    $cpattern = '/^(QA76|TK5105)/';
			    $listLabel = 'Computer Science';
			    break;
			case 'food':
                $cpattern = '/^(TX([3][4-9]|[4-9])|(QP14[1-6])|RA784|RJ206)/';
			    $listLabel = 'Food & Cooking';
			    break;
			case 'lgbt':
			    $cpattern = '/^(HQ7[3-7][\s|\.]|N72\.H64|PN1995\.9\.(H55|I43)|PS153 \.S39|RC883|GV708\.8|HV6250\.4\.H66)/';
			    $listLabel = "LGBTQ Studies";
			    break;
			case 'mathstat':
			    $cpattern = '/^QA[123456]|^QA9[3456789]|^HA/';
			    $listLabel = 'Math & Statistics';
			    break;
			case 'enviro':
			    $cpattern = '/^(GE|GF|HC79\.E5|TJ|TD|QC903)/';
			    $listLabel = 'Environment & Sustainability';
			    break;
			case 'tafilm':
				$cpattern = '/^(PN1[56789]\d\d|PN2\d\d\d|PN3[012]\d\d)/';
				$listLabel = 'Theatre, Film, Television';
				$limit = 'smdvd';
				break;
			    default:
			    $listLabel = '';
                            }
			if ($location !== $limit) {
                        if (preg_match($cpattern, $callno) === 1) {
                            writeItems($author, $titleLine, $subjects, $loc, $callno, $isbnLine, $imgsrc, 'list');
                                }
                                }
		}
                elseif (!empty($scLoc)) { // used for dvds, reserves, ref
                    if (strpos($location, $scLoc)===0) {
                        writeItems($author, $titleLine, $subjects, $loc, $callno, $isbnLine, $imgLink, 'location');
                        }
                        }
                elseif (!empty($regex)) { // useful in testing different callnumber regexes. Leaving it in does no harm
                    if (preg_match('/' . $regex . '/', $callno) === 1) {
                        writeItems($author, $titleLine, $subjects, $loc, $callno, $isbnLine, $imgLink, 'regex');
                    }
                    }
               elseif (isset($complete)) { // default view - doesn't include images
                       writeItems($author, $titleLine, $subjects, $loc, $callno, $isbnLine, $imgLink, 'complete');
                    }
                    
                    
                    else {
                       $homeReg = array("^PN19", "^E18", "^N[CD]", "^H[FG]", "^D");
                       $rand_reg = $homeReg[mt_rand(0, count($homeReg) - 1)];
                   //    $rand_reg = array_rand($homeReg);
                        if (preg_match("/" . $rand_reg . "/", $callno))  {
			writeItems($author, $titleLine, $subjects, $loc, $callno, $isbnLine, $imgLink, 'default');
                    
                        }

                        }
                        
                    }
    echo '</ul>';
    

    echo '</div>'; // ends new books section

            // create hidden divs on the page to be used by javascript
     echo '<div id="page-labels" class="hidden"><div id="call-no-prefix">' .$callNoPrefix . '</div><div id="query">' .$query . '</div><div id="special-list">' . $listLabel . '</div></div>';
    echo '<div id="books-page" class="hidden">' . $bookspage . '</div>';
    echo '<div id="ebooks-flag" class="hidden">' . $ebooks . '</div>';
    if(!empty($query)) { // display onesearch box when a search has been done
        echo '<hr class="beforehr10 afterhr10">';
        echo '<img class="alignleft" alt="OneSearch; Los Rios Libraries" src="//www.scc.losrios.edu/library/wp-content/blogs.dir/109/files/2015/11/ONE_SEARCH_SCC_logo.png"><p class="extend">Extend your search! <label for="extend-query">Check OneSearch for more items</label>';
        echo '<form id="extend-form" action="http://search.ebscohost.com" method="get"><input type="text" name="bquery" id="extend-query" value="' . $query . '"> <input name="authtype" type="hidden" value="ip,guest" /><input name="custid" value="sacram" type="hidden"><input name="groupid" type="hidden" value="main" > <input name="profile" type="hidden" value="eds" > <input name="direct" type="hidden" value="true" >  <input name="site" type="hidden" value="eds-live" > <input name="scope" type="hidden" value="site" > ';
        echo '<input type="submit" value="search" >';
        echo '<input type="hidden" name="cli0" value="FC"><div><input type="checkbox" checked="checked" id="cat-only" name="clv0" value="Y"><label for="cat-only"> Books & Videos Only </label></div></form>';
        }
	echo '<p id="img-attribution">Book images from <a href="//www.openlibrary.org/">Open Library</a>, EBSCO, or <img src="http://www.scc.losrios.edu/library/wp-content/blogs.dir/109/files/2015/11/powered_by_google_on_white.png" alt="Powered by Google"></p>';
            // echo '<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>';
    echo '<script  src="http://wserver.scc.losrios.edu/~library/wp-res/smartpaginator.js"></script>';
    echo '<script src="http://wserver.scc.losrios.edu/~library/new-acquisitions/res/new-books-scripts.js"></script>';
        ?>