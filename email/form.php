<?php

// show errors for debugging
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
$books = $_POST['booklist'];
$label = $_POST['label'];
$url = $_POST['url'];
$title = $_POST['title'];
if (!isset($books)) {
    
    echo "This page's only function to email new book lists. If you are seeing this message, you have likely reached this page in error.";
}
else if (strpos($books, 'ebscohost') === false)
{
    
    echo 'This page only function to email new book lists. If you are seeing this message, you have likely reached this page in error.';
}
else {

 $books = preg_replace('/<img.{70,90}><\/a>/', '</a>', $books);
 $books = str_replace('target="_blank"', '', $books);
 $books = str_replace('style="display: none;"', '', $books);

 
 include_once('includes/head.php');
?>


    <h1>Email this list</h1>
    <p>Your email will contain the list of items below.</p>
    <form id="email-list">
        <label for="address">Send List To:</label>
        <input type="text" id="address" value="">
            <!-- use bookmarklet to allow sender and message -->
            <input type="text" value="" class="hidden" id="additional">
            <input type="submit" value="submit">
        
    </form>
    <div id="the-list">
    <ul>
<?php
echo $books;
?>

</ul>
    
                      </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
        $('.subj-list, .img-link, .isbn').remove();
        $('#the-list ul, #the-list a, #the-list li, #the-list div').removeAttr('class').removeAttr('title');
 //       $('.subj-list').remove();
        $('.new-book').removeAttr('style');
        $('#the-list a').before('<div class="remove"><button title="remove this item from the list" type="button">x</button></div>');
        var removeList = $('.remove button');
        removeList.hover(function() {

            $(this).parents('li').addClass('removing');
            }, function() {
            $(this).parents('li').removeClass('removing');
           } );

        removeList.on('click', function() {
            $(this).parents('li').remove();
            
        });
        
        function post(path, parameters) {
    var form = $('<form></form>');

    form.attr("method", "post");
    form.attr("action", path);
//	form.attr("target", "_blank");

    $.each(parameters, function(key, value) {
        var field = $('<input></input>');

        field.attr("type", "hidden");
        field.attr("name", key);
        field.attr("value", value);

        form.append(field);
    });

    // The form needs to be a part of the document in
    // order for us to be able to submit it.
    $(document.body).append(form);
    form.submit();
}

$('#email-list').on('submit', function(e) {
e.preventDefault();
var emailRegx = /^\S+@\S+\.\S+$/i;
var emailEntry = $('#address');
var theForm = $('#email-list');
if ($('#additional').val() !== '') {
 return false;
}
else if (emailRegx.test(emailEntry.val()) === false) {
    if ($('#error').length === 0) {

    
  $('<div id="error">Please enter a valid email address</div>').appendTo(theForm);
  }
  emailEntry.select();
}
/*
else if ($('#message').val().search(/(href|www|\.(com|org|net|biz))/) > -1) {
    if ($('#error').length === 0) {
    $('<div id="error">You may not post links via this form</div>').appendTo(theForm);
    }
}
*/
else {
    $('.remove').remove();
ga('send', 'event', 'new books lists', 'email list');
				post('success.php', {booklist: $('#the-list').html(), to: $('#address').val(), from: $('#sender').val(), message: $('#message').val(), spam: $('#additional').val(), label: '<?php echo urlencode($label); ?>', url: '<?php echo $url; ?>', title: '<?php echo $title; ?>' });
				
	}
        });
        
       
    </script>
</body>
</html>
<?php
}
?>
