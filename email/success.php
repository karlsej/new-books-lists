<?php
// show errors for debugging
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
$sender = $_POST['from'];
$recipient = $_POST['to'];
$list = $_POST['booklist'];
$addedMessage = $_POST['message'];
$label = $_POST['label'];
$url = $_POST['url'];
$spam = $_POST['spam'];
$title = $_POST['title'];
$ip = $_SERVER['REMOTE_ADDR'];

$headers = "From: SCC Library<no-reply-library@losrios.edu>\r\n";
$headers .= "Reply-To: " . $sender . "\r\n";

$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
$headers .= "Content-Transfer-Encoding:base64 \r\n";

$message = "<html><head>";
$message .= "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n";
$message .= '<style>body {font-family:Verdana, Arial, sans-serif;}  li {margin-bottom:.5em;} div {margin-bottom:0;} table, tr, td {border:none;} a {color:#8A0028; font-weight:bold; text-decoration:none;}</style>';
$message .= '</head><body style="width:90%; padding:10px 4%; font-family:Calibri, Verdana, sans-serif; font-size:11pt;">';
if ($sender !== '') {
$message .= '<p>Sent by: ' . $sender;
}
$message .= '<table style="width:600px;"><tr><td style="width:70%;"><h1 style="font-size:14px;">Items from the SCC Library Collection</h1></td><td style="width:30%"><a href="http://www.scc.losrios.edu/library/"><img alt="Sacramento City College Library"  src="http://www.library.losrios.edu/scc/logos/library-color-sm.png"></a></td><tr></table>';
// $message .= '<p>' . $label . '</p>';
if ($addedMessage !== '') {
$message .= '<p>' . $addedMessage . '</p>';
}
$message .= '<p>View this list online at <a href="' . $url . '">' .$url . '</a></span></p>';
$message .= $list;



$message .= '<p>Sent from IP address: <span>' .$ip . '</span></p>';
$message .= "</body></html>";
if ($spam !== '') {
    echo 'thou art a spammer.';
    
}
else {
    $messagebody = rtrim(chunk_split(base64_encode($message)));
mail($recipient, 'New Books from the SCC Library', $messagebody, $headers);
// echo $headers . $message;
}
include_once('includes/head.php');

?>

<p>Your email was successful.</p>
<p>Redirecting...</p>
<script>
    setTimeout(function() {
       location.href = '<?php echo $url; ?>'; 
        
    }, 3000);
    
</script>
</div>
</body>
</html>