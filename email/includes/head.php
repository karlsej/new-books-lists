<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex">
    <title>Emailing <?php echo $title ?></title>
    <style>
    body {font-family: Verdana, Arial, sans-serif;}
    a {color:#8A0028; font-weight:bold; text-decoration:none;}
    .hidden {display:none;}
    #container {width:600px; margin:0 auto; max-width:100%;}
    li {margin-bottom:.5em;}
    .author, .item-callno {display:block;}
    label {display:block;}
    input[type=text] { border:2px solid #ccc; padding: 2px;}
    input[type=submit] {display:block;}
    .remove {float:right;}
    .removing {background:#ccc; border:1px solid red;}
    button {cursor: pointer;}
   textarea { width: 80%; height: 120px;border: 2px solid #ccc; padding: 5px; text-align:left; }

    </style>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-15285327-1', 'auto');
  ga('send', 'pageview');
  ga('set', 'anonymizeIp', true);
</script>
</head>

<body>
    <div id="container">