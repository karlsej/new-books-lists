jQuery(document).ready(function($) {
  var booksPage = $('#books-page').text();
  var recordsNo = $('.new-book').length;
  // "back to full list" div is hidden by default. unhide it when it's not the main page
  var url = window.location.href;
  var showAll = $('#show-all');

  function singlePage() { // hide pagination when user clicks show all
    $('#pager').smartpaginator({
      totalrecords: $('.new-book').length,
      recordsperpage: 20000,
      datacontainer: 'scc-new-book-list',
      dataelement: '.new-book'
    });
    showAll.hide();
    $('.btn:contains("Next")').attr('style', 'display:none !important');
    $('.btn:contains("Prev")').attr('style', 'display:none !important');
    var singPgStr = '#singlepage';
    if (url.indexOf(singPgStr) === -1) {

      url = url.replace(/$/, singPgStr);
      window.location.href = url;
    }
  }
  if (url.indexOf('?') > -1) {
    if (url.indexOf('#singlepage') > -1) {
      singlePage();
    } else {
      $('#pager').smartpaginator({
        totalrecords: recordsNo,
        recordsperpage: 10,
        datacontainer: 'scc-new-book-list',
        dataelement: '.new-book',
        initval: 0,
        length: 5,
        next: 'Next',
        prev: 'Prev',
        first: 'First',
        last: 'Last'
      });
      document.getElementById('full-list').removeAttribute('class');
    }
  } else {
    $('#show-all, #send-list').hide();
  }
  if (url.indexOf('smdvd') > -1) {
    $('.dvd-icon').hide();
  }
  // fill in urls in widget so widget can be sued on different list pages
  var path = window.location.pathname;
  var listURLs = $('#booklist-special a').attr('href');
  listURLs.replace(/^/, path);
  // go to page on changing dropdown
  var callNoPref = $('#call-no-prefix').text();
  $('#callno-browse').val(callNoPref);
  $('#callno-browse').on('change', function() {
    $('#callno-form').submit();
  });
  $('.subj-list li').each(function() {
    // turn subjects into links
    var a = $(this);
    var text = a.html();
    var subjectQ;
    if (text.indexOf('United') !== 0) {
      // so that United States is not a link by itself...
      subjectQ = text.replace(/ --.*/, '');
    } else {
      subjectQ = text;
    }
    var subjectQenc = encodeURIComponent(subjectQ);
    // without the following, parentheses break
    subjectQenc = subjectQenc.replace('(', '&amp;#40;');
    subjectQenc = subjectQenc.replace(')', '&amp;#41;');
   
    var link = booksPage + '/?query=' + subjectQenc;
    a.html(text.replace(/.*/, '<a title="Search new items for: ' + subjectQ + '" href="' + link + '">' + text + '</a>'));
  });
  // hide previous and next links when they shouldn't show.
  var items = $('.new-book');
  var nextButton = $('.btn:contains("Next")');
  var prevButton = $('.btn:contains("Prev")');
  //  var pageOne = $('#pager span:first-of-type');
  prevButton.attr('style', 'display:none !important');
  // message when no results
  if (items.length === 0) {
    var zeroText = '<p id="no-results">Oh no! You got no results. Try a different search or <a href="' + booksPage + '">view the entire list</a>.</p>';
    $('#newwins').after(zeroText);
    nextButton.attr('style', 'display:none !important');
  } else if (items.length < 11) {
    nextButton.attr('style', 'display:none !important');
  } else {
    // show "show all" button when pagination is active

    showAll.removeClass('hidden');
    showAll.on('click', function() {
      singlePage();
    });
  }



  function changeTitles(text) { // make it so heading and title reflect paramaters used
    var title = $('title').html();
    var newTitle = title.replace(/^/, text + ' - ');
    $('title').html(newTitle);
    var pageTitle = $('.entry-title').html();
    var newPageTitle = pageTitle.replace(/^/, text + ' &#8211; ');
    $('.entry-title').html(newPageTitle);
  }
  var queryText = $('#query').text();
  var listText = $('#special-list').text();
  if (callNoPref !== '') {
    if (callNoPref.length === 1) {
      var options = $('#callno-browse option');
      options.each(function() {
        if ($(this).val() === callNoPref) {
          changeTitles($(this).text());
        }
      });
    } else {
      changeTitles('Call Numbers Starting with ' + callNoPref);
    }
  } else if (queryText !== '') {
    changeTitles('Search Results &#8211; ' + queryText);
    $('#kw-search').val(queryText);
  } else if (listText !== '') {
    changeTitles(listText);
  }
  // jump to top when clicking "next"
  $(nextButton.add(prevButton)).on('click', function() {
    $('html, body').animate({
      scrollTop: ($('#show-all').offset().top)
    }, 300);
    hideButton(nextButton, prevButton);
  });

  function hideButton(a, b) {
    var firstNo = $('#pager span:nth-of-type(1)');
    var ofNo = $('#pager span:nth-of-type(3)');
    var totalNo = $('#pager span:nth-of-type(5)');
    var firstNoAsNo = parseInt(firstNo.text()); // apparently I need a radix paramater, need to look into that
    var ofNoAsNo = parseInt(ofNo.text());
    var totalNoAsNo = parseInt(totalNo.text());
    // console.log(ofNoAsNo);
    // console.log(totalNoAsNo);
    if (ofNoAsNo === totalNoAsNo) {
      a.attr('style', 'display:none !important');
    }
    if (firstNoAsNo === 1) {
      b.attr('style', 'display:none !important');
    }
  }
  hideButton(nextButton, prevButton);
  $('#target-setting').on('click', function() {
    var a = $(this);
    var b = $('.new-book-link, #extend-form');
    if (a.is(':checked')) {
      setCookie('newWindowLinks', 'yes', 30);
      b.attr('target', '_blank');
    } else {
      setCookie('newWindowLinks', 'no', 1);
      b.removeAttr('target');
    }
  });
  // Post to the provided URL with the specified parameters, for emailing. http://stackoverflow.com/a/5533477
  function post(path, parameters) {
    var form = $('<form></form>');
    form.attr("method", "post");
    form.attr("action", path);
    //   form.attr("target", "_blank");
    $.each(parameters, function(key, value) {
      var field = $('<input></input>');
      field.attr("type", "hidden");
      field.attr("name", key);
      field.attr("value", value);
      form.append(field);
    });
    // The form needs to be a part of the document in
    // order for us to be able to submit it.
    $(document.body).append(form);
    form.submit();
  }
  $('#send-list').on('click', function() {
    $('#scc-new-book-list img, .subj-list').remove();
    var page = window.location.href;
    if (page.indexOf('ebooks') > -1) {
    	$('.callno').remove();
    }
    post('https://wserver.scc.losrios.edu/~library/new-acquisitions/email/form.php', {
      booklist: $('#scc-new-book-list').html(),
      label: $('#page-labels').text(),
      url: page,
      title: $('.entry-title').text()
    });
  });
  // Google Analytics
  $('.new-book-link').on('click', function() {
    ga('localsiteTracker.send', 'event', 'new books lists', 'click', 'item');
  });
  $('.subj-list a').on('click', function() {
    ga('localsiteTracker.send', 'event', 'new books lists', 'click', 'subject');
  });
  $('#send-list').on('click', function() {
    ga('localsiteTracker.send', 'event', 'new books lists', 'click', 'email list');
  });
  $('#show-all').on('click', function() {
    ga('localsiteTracker.send', 'event', 'new books lists', 'click', 'single-page view');
  });
  if ($('#ebooks-flag').text() === 'yes') {
    $('.callno, .books-only').hide();
  }
  $("img").error(function() {
    $(this).hide();
    // or $(this).css({visibility:"hidden"}); 
  });
  // Set the api variable
  var googleAPI = "https://www.googleapis.com/books/v1/volumes?key=AIzaSyDCwiS6wHiD9HMoJtT4nVFnh5YaLMCmZo0&q=isbn:";
  $('.new-book').each(function() {
    var a = $(this);
    var isbnDiv = a.find('.isbn');
    var isbn = isbnDiv.text();
    var linkEl = a.find('.new-book-link');
    // get link
    var link = linkEl.attr('href');

    function makeOpenLibLink(link, isbn) {
    	if ((isbn !== '') && (isbn !== '0')) {
      var openLibLink = $('<img alt="" class="jacket" src="//covers.openlibrary.org/b/isbn/' + isbn + '-M.jpg?default=false" onerror="this.style.display=\'none\'; this.setAttribute(\'src\', \'http://contentcafe2.btol.com/ContentCafe/jacket.aspx?UserID=ebsco-test&Password=ebsco-test&Return=1&Type=M&Value=' + isbn + '\'); this.removeAttribute(\'style\'); this.setAttribute(\'onerror\', \'this.style.display=&quot;none&quot;\');">');
      openLibLink.prependTo(linkEl);
      checkCookie();
    	}

    }

    if ((isbn !== '') && (!(a.find('.dvd-icon').length))) {
      if (((a.find('.jacket').length) > 0) === false) {
        console.log('no jacket');


        //	console.log('hey!');
        // Make a ajax call to get the json data as response.
        $.getJSON(googleAPI + isbn, function(response) {
          //      console.log(googleAPI + isbn);
          // In console, you can see the response objects
          //      console.log("JSON Data: " + response.items);
          if (response.items) {
            // Loop through all the items one-by-one
            for (var i = 0; i < response.items.length; i++) {
              // set the item from the response object
              var item = response.items[i];
              if (item.volumeInfo.imageLinks) {
                //code
                var thumb = item.volumeInfo.imageLinks.thumbnail;
                if (thumb) {
                  thumb = thumb.replace('&edge=curl', '');
                  var image = $('<img alt="" class="jacket gImage" src="' + thumb + '">');
                  image.prependTo(linkEl);
                  checkCookie();
                }
              }

            }
          } else {

            makeOpenLibLink(link, isbn);
          }
        }).fail(function() { // if we hit the api limit, it goes to openLibrary
          makeOpenLibLink(link, isbn);
        });

      }
    }
    else if (a.find('.dvd-icon').length) {
      makeOpenLibLink(link, isbn);
    }


  });
  checkCookie();
});
// cookies
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = 'expires=' + d.toUTCString();
  document.cookie = cname + '=' + cvalue + '; ' + expires + '; path=/;domain=losrios.edu';
}

function getCookie(cname) {
  var name = cname + '=';
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1);
    if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
  }
  return '';
}

function checkCookie() {
  var newWins = getCookie('newWindowLinks');
  if (newWins === 'yes') {
    document.getElementById('target-setting').checked = true;
    var links = document.querySelectorAll('.new-book-link');
    var extendForm = document.getElementById('extend-form');
    if (links) {
      for (var a = 0; a < links.length; a++) {
        links[a].setAttribute('target', '_blank');
      }
    }
    if (extendForm) {
      extendForm.setAttribute('target', '_blank');
    }
  }
  if (getCookie('onesearchDomain') === 'proxy') {
    jQuery('.new-book a').each(function() {
      var a = jQuery(this).attr('href');
      var b = a.replace('search.ebscohost.com/', '0-search.ebscohost.com.lasiii.losrios.edu/');
      jQuery(this).attr('href', b);
      });
  }
}
